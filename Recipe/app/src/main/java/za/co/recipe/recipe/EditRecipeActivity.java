package za.co.recipe.recipe;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import za.co.recipe.recipe.db.ApplicationModel;
import za.co.recipe.recipe.db.DBAdapter;
import za.co.recipe.recipe.utils.BitmapUtils;


public class EditRecipeActivity extends Activity
{

    public final static String EDITED_ROW_ID = "za.co.recipe.MainActivity.EDITED_ROW_ID";
    private static int RESULT_LOAD_IMAGE = 1;

    private ApplicationModel appModel;
    private EditText titleTxt;
    private EditText recipeTxt;
    private EditText methodTxt;
    private ImageView imgView;
    private String picturePath = "";
    private String title;
    private String recipe;
    private String method;
    private String picPath;
    private long currRowID;
    private ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recipe);

        appModel = ApplicationModel.getInstance();
        //appModel.openDB(this);

        Intent intent = getIntent();
        currRowID = intent.getLongExtra(RecipeActivity.CUR_ROW_ID, 1);
        imageView = (ImageView) findViewById(R.id.pic);

        Cursor cursor = appModel.myDb.getRow(currRowID);
        displayRecordSet(cursor);
    }

    private void displayRecordSet(Cursor cursor)
    {
        String message = "";
        // populate the message from the cursor

        // Reset cursor to start, checking to see if there's data:
        if (cursor.moveToFirst()) {
            do {
                // Process the data:
               // int id = cursor.getInt(DBAdapter.COL_ROWID);
               title = cursor.getString(DBAdapter.COL_TITLE);
                recipe = cursor.getString(DBAdapter.COL_RECIPE);
                method = cursor.getString(DBAdapter.COL_METHOD);
                picPath = cursor.getString(DBAdapter.COL_PIC);
                // Append data to the message:
                //message += "id=" + id
                //        +", name=" + title
                 //       +", #=" + recipe
                        //+", Colour=" + favColour
                 //       +"\n";
            } while(cursor.moveToNext());
        }

        // Close the cursor to avoid a resource leak.
        cursor.close();
        titleTxt = (EditText)findViewById(R.id.edit_title_edit_mode);
        recipeTxt = (EditText)findViewById(R.id.edit_recipe_edit_mode);
        methodTxt = (EditText)findViewById(R.id.edit_method_edit_mode);
        imgView = (ImageView)findViewById(R.id.pic);

        titleTxt.setText(title);
        recipeTxt.setText(recipe);
        methodTxt.setText(method);

        Bitmap bmp = BitmapUtils.setImageToImageView(picPath, 512);

        imgView.setImageBitmap(bmp);
        //imgView.setImageBitmap(BitmapFactory.decodeFile(picPath));

        // displayText(message);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        //appModel.closeDB();
    }

    public void saveChanges(View v)
    {

        String updatedTitle = titleTxt.getText().toString();
        String updatedRecipe = recipeTxt.getText().toString();
        String updatedMethod = methodTxt.getText().toString();
        //String updatedImg = imageView.getTag(R.string.tag_img_path).toString();

        appModel.myDb.updateRow(currRowID,updatedTitle,updatedRecipe,updatedMethod,picPath);
        Intent intent = new Intent(this, RecipeActivity.class);
        intent.putExtra("UID","EditRecipe");
        intent.putExtra(EDITED_ROW_ID,currRowID);
        startActivity(intent);
    }

    public void saveImage(View v)
    {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data)
        {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picPath = cursor.getString(columnIndex);
            cursor.close();


            imageView.setImageBitmap(BitmapFactory.decodeFile(picPath));

            imageView.setTag(R.string.tag_img_path,picPath);

        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_recipe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
