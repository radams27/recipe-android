package za.co.recipe.recipe;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import za.co.recipe.recipe.utils.BitmapUtils;
import za.co.recipe.recipe.vo.ListItemVO;

/**
 * Created by reza on 2014-09-17.
 */
public class ListLoader extends ArrayAdapter/*SimpleCursorAdapter*/ {

    private Context context;
    private MainActivity.ViewHolder viewHolder;
    private LruCache<String, Bitmap> mMemoryCache;

    public ListLoader(Context ctx, int resource,int id,List<ListItemVO> objects/*Context context, int layout,Cursor cursor,String[] fromFieldNames, int[]toViewIDS*/)
   {
       super(ctx,resource, id,objects);
       context = ctx;
        //super(context,layout,cursor,fromFieldNames,toViewIDS);

       final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
       final int cacheSize = maxMemory / 8;

       mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
           @Override
           protected int sizeOf(String key, Bitmap bitmap) {
               // The cache size will be measured in kilobytes rather than
               // number of items.
               return bitmap.getByteCount() / 1024;
           }
       };



   }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

   @Override
    public View getView(int position, View convertView, ViewGroup parent){

        ///View item = super.getView(position, convertView, parent);

       //viewholder pattern
      // MainActivity.ViewHolder viewHolder;

       ListItemVO li = (ListItemVO) getItem(position);

       if(convertView==null)
       {
           LayoutInflater inflater = ((Activity) context).getLayoutInflater();
           convertView = inflater.inflate(R.layout.item_layout, parent, false);

           // well set up the ViewHolder

           viewHolder = new MainActivity.ViewHolder();

           viewHolder.txt = (TextView) convertView.findViewById(R.id.item_name);
           viewHolder.icon = (ImageView) convertView.findViewById(R.id.list_pic);
           viewHolder.id = li.id();
           viewHolder.position = position;
           viewHolder.color = li.color();


           // store the holder with the view.
           convertView.setTag(viewHolder);



       }else{
           // we've just avoided calling findViewById() on resource everytime
           // just use the viewHolder
           viewHolder = (MainActivity.ViewHolder) convertView.getTag();
           viewHolder.id = li.id();

           viewHolder.icon.setImageBitmap(null);
           //viewHolder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.add_icon));
           //viewHolder.icon.setVisibility(View.INVISIBLE);
           if(viewHolder.task!= null)
             viewHolder.task.cancel(true);


       }



      if(li != null){
            viewHolder.txt.setText(li.title());
            //viewHolder.txt.setTag(li.id());
            //Bitmap bmp = BitmapUtils.setImageToImageView(li.pic());
           viewHolder.imageUrl = li.pic();
           // viewHolder.icon.setImageBitmap(bmp);
            //viewHolder.icon.setTag(li.pic());

            String currString = viewHolder.txt.getText().toString();

            if(currString.length() > 1) {
                String formattedString = currString.substring(0, 1).toUpperCase() + currString.substring(1).toLowerCase();
                viewHolder.txt.setText(formattedString);
            }

            /*if(getItemId(position) % 2 == 0)
            {
                int color = Color.parseColor("#FFFFFF");
                viewHolder.color = color;

            }*/

          final Bitmap bitmap = getBitmapFromMemCache(String.valueOf(li.id()));
          if (bitmap != null) {
              viewHolder.icon.setImageBitmap(bitmap);
              Log.d("using cache >>> with id: ", String.valueOf(li.id()));
          } else {

              MyTask task = new MyTask(viewHolder.icon);
              task.execute(viewHolder);//.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,viewHolder);
              viewHolder.task = task;

          }

      }

       return convertView;
    }

    class MyTask extends AsyncTask<MainActivity.ViewHolder,String,Bitmap>
    {
        private SimpleCursorAdapter cAdapter;
        private Cursor cursorAsync;
        private int count = 0;
        private ArrayAdapter<ListItemVO> adapter;
        private MainActivity.ViewHolder v;
        private  WeakReference<ImageView> imageViewReference;

        public MyTask(ImageView imageView)
        {
            imageViewReference = new WeakReference<ImageView>(imageView);
        }
        @Override
        protected void onPreExecute(){
            //adapter = (ArrayAdapter<ListItemVO>) list.getAdapter();
            //viewHolder.icon.setVisibility(View.INVISIBLE);
        }

        @Override
        protected Bitmap doInBackground(MainActivity.ViewHolder...params)
        {
            v = params[0];

            //for(ListItemVO item:listItems)
            //{
            //  String[] li = {item.title(),item.pic()};
            // publishProgress(li);
            // Bitmap bmp = BitmapUtils.setImageToImageView(item.pic());
            // }

           // if(isCancelled())
             //   return null;
            //else {
                Bitmap bmp = BitmapUtils.setImageToImageView(v.imageUrl, 128);
                if(bmp != null)
                     addBitmapToMemoryCache(String.valueOf(v.id), bmp);
                return bmp;
            //}






        }

        @Override
        protected void onProgressUpdate(String...values)
        {
            //ListItemVO li = new ListItemVO(0,values[0],"","",values[1]);
           // adapter.add(li);
            //View v = list.getChildAt(count);
            //View newView = (View) cAdapter.getView(count,v,list);
            //TextView tv = (TextView) newView.findViewById(id.item_name);
            // ImageView pic = (ImageView) adapter.

            //Bitmap bmp = BitmapUtils.setImageToImageView(values[0]);

            //v.setImageBitmap(bmp);
            //pic.setImageBitmap(bmp);
        }

        @Override
        protected void onPostExecute(Bitmap result)
        {
            // Close the cursor to avoid a resource leak.
            super.onPostExecute(result);
           if(imageViewReference != null && result != null)
           {

               v.icon = imageViewReference.get();
               if(v.icon != null)
               {
                   Log.w("id", "" + v.position);
                   //v.icon.setVisibility(View.VISIBLE);
                   v.icon.setImageBitmap(result);

                   Animation fadeIn = new AlphaAnimation(0, 1);  // the 1, 0 here notifies that we want the opacity to go from opaque (1) to transparent (0)
                   fadeIn.setInterpolator(new AccelerateInterpolator());
                   fadeIn.setStartOffset(0); // Start fading out after 500 milli seconds
                   fadeIn.setDuration(200); // Fadeout duration should be 1000 milli seconds
                   v.icon.setAnimation(fadeIn);
               }
           }
            //v.txt.setBackgroundColor(v.color);
            //v.icon.setBackgroundColor(v.color);


        }
    }



}
