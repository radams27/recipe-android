package za.co.recipe.recipe;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import za.co.recipe.recipe.db.ApplicationModel;
import za.co.recipe.recipe.db.DBAdapter;
import za.co.recipe.recipe.vo.ListItemVO;

import static za.co.recipe.recipe.R.id;
import static za.co.recipe.recipe.R.layout;


public class MainActivity extends Activity /*implements LoaderManager.LoaderCallbacks<Cursor>*/{

    public final static String SELECTED_TITLE = "za.co.recipe.MainActivity.SELECTED_TITLE";
    public final static String SELECTED_RECIPE = "za.co.recipe.MainActivity.SELECTED_RECIPE";
    public final static String SELECTED_METHOD = "za.co.recipe.MainActivity.SELECTED_METHOD";
    public final static String SELECTED_ROW_ID = "za.co.recipe.MainActivity.SELECTED_ROW_ID";

    private TextView titleTxt;
    private TextView recipeTxt;

    private int padding;
    private int initialx;
    private int currentx;
    private int prevBottom =0;
    private View prevListItem;

    private ListView list;

    boolean mSwiping = false;
    boolean mItemPressed = false;
    private  ArrayList<ListItemVO> listItems;
    private SimpleCursorAdapter cursorAdapter;
    private ProgressBar progressBar;
    private int count = 0;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    HashMap<Long, Integer> mItemIdTopMap = new HashMap<Long, Integer>();

    private static final int SWIPE_DURATION = 250;
    private static final int MOVE_DURATION = 150;

    //DBAdapter myDb;
    private ApplicationModel appModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

       // ActionBar bar = getActionBar();
        //for color
               // bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#26a006")));
        //for image
       // bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.settings_icon));
       progressBar = new ProgressBar(this);
        progressBar.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        progressBar.setIndeterminate(true);



        Intent intent = getIntent();
        String title = intent.getStringExtra(AddRecipeActivity.TITLE);
        String recipe = intent.getStringExtra(AddRecipeActivity.RECIPE);
        String method = intent.getStringExtra(AddRecipeActivity.METHOD);
        String pic = intent.getStringExtra(AddRecipeActivity.PIC);

       // mBackgroundContainer = (BackgroundContainer) findViewById(id.listViewBackground);

        setContentView(layout.activity_main);

       // titleTxt = (TextView) findViewById(R.id.title);

        appModel = ApplicationModel.getInstance();
        appModel.openDB(this);



        if(title != null && recipe != null) {
            appModel.addRecord(title, recipe, method,pic);

        }

        Cursor cursor = appModel.myDb.getAllRows();
        displayRecords(cursor);



       // recipeTxt = (TextView) findViewById(R.id.recipe);
       // if(title != null)
       //     titleTxt.setText(title);
       // if(recipe != null)
       //     recipeTxt.setText(recipe);


    }

    /*private void openDB()
    {
        myDb.getInstance(this);
        myDb.open();
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // appModel.closeDB();
    }

   /* private void closeDB()
    {
        myDb.close();
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        Cursor cursor = appModel.myDb.getAllRows();
        displayRecords(cursor);
        if(searchView != null) {
            searchView.onActionViewCollapsed();  //collapse your ActionView
            searchView.setQuery("", false);

            MenuItemCompat.collapseActionView(searchMenuItem);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
       // super.onRestoreInstanceState(savedInstanceState);
        Cursor cursor = appModel.myDb.getAllRows();
        displayRecords(cursor);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void displayRecords(Cursor cursor)
    {


        //Cursor cursor = appModel.getAllRows();
        //deprecated
       // startManagingCursor(cursor);
        count = 0;
        listItems = new ArrayList<ListItemVO>();




        if (cursor.moveToFirst()) {
            do {
                // Process the data:
                int id = cursor.getInt(DBAdapter.COL_ROWID);
                String title = cursor.getString(DBAdapter.COL_TITLE);
                String recipe = cursor.getString(DBAdapter.COL_RECIPE);
                String method = cursor.getString(DBAdapter.COL_METHOD);
                String pic = cursor.getString(DBAdapter.COL_PIC);
                //String favColour = cursor.getString(DBAdapter.COL_FAVCOLOUR);
                count ++;
                int color = Color.parseColor("#F6F6F6");

                ListItemVO listItemVO = new ListItemVO(id,title,recipe,method,pic,color);
                //String[] strings = {title,pic};
                listItems.add(listItemVO);

                String message = "";
                // Append data to the message:
                message += "id=" + id
                        +", title=" + title
                        +", recipe=" + recipe
                        +", method=" + method
                        +"\n";

                Log.d("db",message);
            } while(cursor.moveToNext());
        }



        //String[] fromFieldNames = new String[]{DBAdapter.KEY_TITLE};
       // int[] toViewIDs = new int[] {id.item_name};

        //SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
        /* cursorAdapter = new ListLoader(
                this, layout.item_layout,
                null, //// We pass null for the cursor, then update it in onLoadFinished()
                fromFieldNames,
                toViewIDs
        );*/

       // stopManagingCursor(cursor);
        cursor.close();



        ArrayAdapter<ListItemVO> listAdapter = new ListLoader(this, layout.item_layout, id.item_name, listItems);

        list = (ListView) findViewById(id.listView);
        //list.setAdapter(cursorAdapter);
        list.setAdapter(listAdapter);
        registerForContextMenu(list);
        list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        registerListClickCallbacks();

       // ImageView img = (ImageView) cursorAdapter.get
       // img.setRotation(50);

        //ImageBg img = (ImageBg) findViewById(id.list_pic);

       // img.init(this);
       // getLoaderManager().initLoader(0, null, this);

        //getLoaderManager().initLoader(0,null,this);


        //displayRecordSet(cursor);
    }

    private void registerListClickCallbacks()
    {

        list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {



            @Override
            public void onItemClick(AdapterView<?> adapterView, View viewClicked, int position, long idInDB)
            {
                String title = "";
                String recipe = "";
                String method = "";


                ViewHolder v = (ViewHolder) viewClicked.getTag();
                long rowID = v.id;
                Log.d(" >> id:", "" + v.id);


                Intent intent = new Intent(MainActivity.this, RecipeActivity.class);

                intent.putExtra("UID", "Main");

                intent.putExtra(SELECTED_ROW_ID, rowID);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doSearch(query);
            Log.d("search >>><<<<",query);
        }
    }

    private void doSearch(String query)
    {
        Cursor cursor = appModel.myDb.searchDB(query);
        displayRecords(cursor);
    }




    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);
        //menu.setHeaderTitle("Test");


    }

    @Override
    public boolean onContextItemSelected (MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (item.getItemId() == id.list_item_edit) {
            ViewHolder v =  (ViewHolder) info.targetView.getTag();
            long rowIDForEdit = v.id;
            Intent intent = new Intent(this, EditRecipeActivity.class);
            intent.putExtra(RecipeActivity.CUR_ROW_ID, rowIDForEdit);
            startActivity(intent);        }
        else if (item.getItemId() == id.list_item_delete) {


            ViewHolder v =  (ViewHolder) info.targetView.getTag();
            long rowIDForDeletion = v.id;//info.id ;
            appModel.myDb.deleteRow(rowIDForDeletion);
            Cursor cursor = appModel.myDb.getAllRows();
            displayRecords(cursor);
            //String val = String.valueOf(info.id);
            //Toast toast = Toast.makeText(this,val, Toast.LENGTH_SHORT);
           /// toast.show();
            return true;

        } else if (item.getItemId() == id.list_item_cancel) {

            return true;

        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        searchMenuItem = menu.findItem(id.menu_search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s)
            {
                Cursor cursor = appModel.myDb.searchDB(s);
                displayRecords(cursor);
                return false;
            }
        });



        return true;
    }

   /* @Override
    public void onBackPressed() {
        if (searchView.isShown()){
            searchView.onActionViewCollapsed();  //collapse your ActionView
            searchView.setQuery("",false);       //clears your query without submit
            //isClosed = true;                     //needed to handle closed by back
        } else{
            super.onBackPressed();
        }
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        switch (item.getItemId())
        {
            case R.id.action_add:
                Intent intent = new Intent(this, AddRecipeActivity.class);
                startActivity(intent);

                return true;
            case R.id.action_clear:
               // Intent intent = new Intent(this, AddRecipeActivity.class);
                //startActivity(intent);
                appModel.myDb.deleteAll();
                Cursor cursor = appModel.myDb.getAllRows();
                displayRecords(cursor);
                return true;
            default:
                return super.onOptionsItemSelected(item);
            // return super.onOptionsItemSelected(item);
        }
        //return super.onOptionsItemSelected(item);
    }



    static class ViewHolder {
        long id;
        TextView txt;
        ImageView icon;
        String imageUrl;
        ProgressBar progress;
        int position;
       int color;
        AsyncTask task;
    }

}
