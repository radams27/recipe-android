package za.co.recipe.recipe.vo;

/**
 * Created by reza on 2014-09-19.
 */
public class ListItemVO
{
    private long _id;
    private String _title;
    private String _recipe;
    private String _method;
    private String _pic;
    private int _color;

    public ListItemVO(long Id, String titleStr, String recipeStr,String methodStr,String pic, int color){
        super();
        _id = Id;
        _title = titleStr;
        _recipe = recipeStr;
        _method = methodStr;
        _pic = pic;
        _color = color;
    }


    public int color()
    {
        return _color;
    }
    public String title()
    {
        return _title;
    }

    public String recipe()
    {
        return _recipe;
    }

    public String pic()
    {
        return _pic;
    }

    public String method()
    {
        return _method;
    }

    public long id()
    {
        return _id;
    }
}


