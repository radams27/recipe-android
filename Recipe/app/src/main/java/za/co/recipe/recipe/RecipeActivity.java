package za.co.recipe.recipe;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import za.co.recipe.recipe.db.ApplicationModel;
import za.co.recipe.recipe.db.DBAdapter;
import za.co.recipe.recipe.utils.BitmapUtils;
import za.co.recipe.recipe.vo.ListItemVO;


public class RecipeActivity extends FragmentActivity implements RecipeFragment.OnFragmentInteractionListener,PagerAdapter.OnUpdatePage
{

    public final static String CUR_ROW_ID = "za.co.recipe.RecipeActivity.CUR_ROW_ID";

    private ApplicationModel appModel;
    private TextView recipeTxt;
    private TextView methodTxt;
    private ImageView picView;
    private long rowID;
    private String title;
    private String recipe;
    private String method;
    private String pic;
    private boolean isRowIDSet = false;
    private ViewPager pager;
    private ArrayList<ListItemVO> listItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        Intent intent = getIntent();
       // String title = intent.getStringExtra(AddRecipeActivity.TITLE);
        //String recipeText = intent.getStringExtra(MainActivity.SELECTED_RECIPE);
       // String methodText = intent.getStringExtra(MainActivity.SELECTED_METHOD);
        if(intent != null)
        {
            String strdata = intent.getStringExtra("UID");//getExtras().getString("UID");
           if(strdata != null && strdata.equals("Main"))
           {
               rowID = intent.getLongExtra(MainActivity.SELECTED_ROW_ID, 1);
               isRowIDSet = true;
           }
           else if(strdata != null && strdata.equals("EditRecipe"))
           {
               rowID = intent.getLongExtra(EditRecipeActivity.EDITED_ROW_ID, 1);
               isRowIDSet = true;
           }

        }

        String test = "test";
        if(isRowIDSet)
        {
            String t = test;
        }

        appModel = ApplicationModel.getInstance();
       // appModel.openDB(this);

        /*
        recipeTxt = (TextView) findViewById(R.id.recipe_text);
        methodTxt = (TextView) findViewById(R.id.method_text);
        picView = (ImageView) findViewById(R.id.recipe_pic);


        methodTxt.setMovementMethod(new ScrollingMovementMethod());*/

        if(!isRowIDSet) {
            rowID = appModel.rowID;
        }

       // Cursor cursor = appModel.myDb.getRow(rowID);

        ///displayRecordSet(cursor);
        Cursor cursor = appModel.myDb.getAllRows();
        getArray(cursor);

        pager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), listItems);
        pager.setAdapter(pagerAdapter);



        int i = 0;
        while (listItems.get(i).id() != rowID) {
            Log.d(String.valueOf(listItems.get(i).id()), String.valueOf(rowID));
            i++;
        }

        pager.setCurrentItem(i);
        PagerTabStrip strip = PagerTabStrip.class.cast(findViewById(R.id.frag_title));
        strip.setDrawFullUnderline(false);



    }

    private void getArray(Cursor cursor)
    {

        //Cursor cursor = appModel.getAllRows();
        //deprecated
        // startManagingCursor(cursor);

        listItems = new ArrayList<ListItemVO>();

        if (cursor.moveToFirst()) {
            do {
                // Process the data:
                int id = cursor.getInt(DBAdapter.COL_ROWID);
                String title = cursor.getString(DBAdapter.COL_TITLE);
                String recipe = cursor.getString(DBAdapter.COL_RECIPE);
                String method = cursor.getString(DBAdapter.COL_METHOD);
                String pic = cursor.getString(DBAdapter.COL_PIC);
                //String favColour = cursor.getString(DBAdapter.COL_FAVCOLOUR);



                ListItemVO listItemVO = new ListItemVO(id,title,recipe,method,pic,1);
                //String[] strings = {title,pic};
                listItems.add(listItemVO);

                String message = "";
                // Append data to the message:
                message += "id=" + id
                        +", title=" + title
                        +", recipe=" + recipe
                        +", method=" + method
                        +"\n";

                Log.d("db", message);
            } while(cursor.moveToNext());
        }
    }


    public void onFragmentInteraction(String uri)
    {
        Log.d("test",uri);
    }

    public void onUpdatePage(int position){
        Log.d("position >>>> ",String.valueOf(position));
    }



    private void displayRecordSet(Cursor cursor)
    {
        String message = "";
        // populate the message from the cursor

        // Reset cursor to start, checking to see if there's data:
        if (cursor.moveToFirst()) {
            do {
                // Process the data:
                // int id = cursor.getInt(DBAdapter.COL_ROWID);
                title = cursor.getString(DBAdapter.COL_TITLE);
                recipe = cursor.getString(DBAdapter.COL_RECIPE);
                method = cursor.getString(DBAdapter.COL_METHOD);
                pic  = cursor.getString(DBAdapter.COL_PIC);

                // Append data to the message:
                //message += "id=" + id
                //        +", name=" + title
                //       +", #=" + recipe
                //+", Colour=" + favColour
                //       +"\n";
            } while(cursor.moveToNext());
        }

        // Close the cursor to avoid a resource leak.
        cursor.close();
        recipeTxt.setText(recipe);
        methodTxt.setText(method);

        if(title != null) {
            if (title.length() > 1 ) {
                String formattedString = title.substring(0, 1).toUpperCase() + title.substring(1).toLowerCase();
                setTitle(formattedString);
            } else {
                setTitle(title);
            }
        }

        Bitmap bmp = BitmapUtils.setImageToImageView(pic, 512);

        picView.setImageBitmap(bmp);
        //picView.setImageBitmap(BitmapFactory.decodeFile(pic));
        // displayText(message);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //appModel.closeDB();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.recipe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        switch (item.getItemId())
        {
            case R.id.action_edit:

                int currIndex = pager.getCurrentItem();


                Intent intent = new Intent(this, EditRecipeActivity.class);
                rowID = listItems.get(currIndex).id();
                appModel.rowID = rowID;

                        intent.putExtra(CUR_ROW_ID,rowID);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
            // return super.onOptionsItemSelected(item);
        }
        //return super.onOptionsItemSelected(item);
    }
}
