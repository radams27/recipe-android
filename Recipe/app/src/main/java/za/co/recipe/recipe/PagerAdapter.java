package za.co.recipe.recipe;

import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import za.co.recipe.recipe.db.ApplicationModel;
import za.co.recipe.recipe.vo.ListItemVO;

/**
 * Created by reza on 2014-10-01.
 */
public class PagerAdapter extends FragmentPagerAdapter
{


    private  long _currRowID;
    private ApplicationModel appModel;
    private ArrayList<ListItemVO> _listItems;

    public PagerAdapter(android.support.v4.app.FragmentManager fm,ArrayList<ListItemVO> listItems)
    {
        super(fm);
        _listItems = listItems;

    }


    @Override
    public android.support.v4.app.Fragment getItem(int position)
    {
        long id = _listItems.get(position).id();
        String title = _listItems.get(position).title();
        String recipe = _listItems.get(position).recipe();
        String method = _listItems.get(position).method();
        String pic = _listItems.get(position).pic();

        Bundle bundle = new Bundle();
        bundle.putLong("id",id);
        bundle.putString("title",title);
        bundle.putString("recipe",recipe);
        bundle.putString("method",method);
        bundle.putString("pic",pic);

       RecipeFragment recipeFragment = new RecipeFragment();
        recipeFragment.setArguments(bundle);
        return recipeFragment;

    }

    @Override
    public int getItemPosition(Object object)
    {
        RecipeFragment recipeFragment = (RecipeFragment) object;
        Bundle data = recipeFragment.getArguments();

        int i = 0;
        while (_listItems.get(i).id() != data.getLong("id")) {
            //Log.d(String.valueOf(_listItems.get(i).id());

            i++;
        }

        if(_listItems.get(i).title() != data.getString("title")){
            return POSITION_NONE;
        }
        if(_listItems.get(i).recipe() != data.getString("recipe")){
            return POSITION_NONE;
        }
        if(_listItems.get(i).method() != data.getString("method")){
            return POSITION_NONE;
        }
        if(_listItems.get(i).pic() != data.getString("pic")){
            return POSITION_NONE;
        }




        return POSITION_UNCHANGED;
    }

    @Override
    public CharSequence getPageTitle(int position){

        String currString = _listItems.get(position).title();


        if(currString.length() > 1) {
            String formattedString = currString.substring(0, 1).toUpperCase() + currString.substring(1).toLowerCase();
            return formattedString;
        }


        return currString;
    }


    @Override
    public int getCount()
    {
        return _listItems.size();

    }

    public interface OnUpdatePage{
        public void onUpdatePage(int position);
    }

}
