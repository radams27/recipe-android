package za.co.recipe.recipe;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import za.co.recipe.recipe.utils.BitmapUtils;


public class AddRecipeActivity extends Activity
{

    public final static String TITLE = "za.co.recipe.AddRecipeActivity.TITLE";
    public final static String RECIPE = "za.co.recipe.AddRecipeActivity.RECIPE";
    public final static String METHOD = "za.co.recipe.AddRecipeActivity.METHOD";
    public final static String PIC = "za.co.recipe.AddRecipeActivity.PIC";

    private static int RESULT_LOAD_IMAGE = 1;
    private String picturePath = "";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_recipe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendMessage(View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        EditText title = (EditText) findViewById(R.id.edit_title);
        String titleTxt = title.getText().toString();

        EditText recipe = (EditText) findViewById(R.id.edit_recipe);
        String recipeTxt = recipe.getText().toString();

        EditText method = (EditText) findViewById(R.id.edit_method);
        String methodTxt = method.getText().toString();

        intent.putExtra(TITLE, titleTxt);
        intent.putExtra(RECIPE, recipeTxt);
        intent.putExtra(METHOD, methodTxt);
        intent.putExtra(PIC, picturePath);
        startActivity(intent);
    }

    public void saveImage(View v)
    {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data)
        {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.pic);

           Bitmap bmp = BitmapUtils.setImageToImageView(picturePath, 512);

            imageView.setImageBitmap(bmp);

           // decodeSampledBitmapFromResource(getResources(), R.id.myimage, 100, 100))
            //imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }



    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // appModel.closeDB();
    }

}
