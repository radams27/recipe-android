package za.co.recipe.recipe.db;

import android.content.Context;
import android.database.Cursor;

/**
 * Created by reza on 2014-09-12.
 */
public class ApplicationModel {

    private static ApplicationModel mInstance= null;
    public DBAdapter myDb;
    public long rowID;

    public static synchronized ApplicationModel getInstance(){
        if(null == mInstance){
            mInstance = new ApplicationModel();
        }
        return mInstance;
    }

    public void addRecord(String title, String recipe,String method, String pic)
    {

        long newId = myDb.insertRow(title, recipe,method,pic);

        // Query for the record we just added.
        // Use the ID:
        Cursor cursor = myDb.getRow(newId);
        //displayRecordSet(cursor);
    }

    public void openDB(Context ctx)
    {

        myDb = myDb.getInstance(ctx.getApplicationContext());
        myDb.open();
    }



    public void closeDB()
    {
        myDb.close();
    }

}
